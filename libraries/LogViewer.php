<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter LogViewer Class
 *
 * This class is and interface to CI's to access to Logs via website
 *
 * @package		CodeIgniter
 * @author		Jacobo Pedrosa
 * @subpackage	Libraries
 * @category	Libraries
 * @link		http://www.jacobopedrosa.com/ci/libraries/logviewer
 * @copyright  Copyright (c) 2015, Jacobo Pedrosa.
 * @version 0.0.1
 * 
 */
	
class CI_LogViewer{
	private $data = array();
	public $CI;

	public $today='';
	public $filter = FALSE;
	public $pages = array();
	public $log_file = '';
	public $url_vals = array();


	function CI_LogViewer(){
		$this->CI = &get_instance();
		$this->CI->load->helper( array('file','url') );	
		$this->CI->load->library( 'pagination' );


		$this->data['params'] = $this->getParams();
		$this->today = 'log-'.date( 'Y-m-d') . '.php';

		$this->data['files']=$this->listFiles();
		
		if( isset( $this->data['params'][ 'file' ] ) and !empty( $this->data['params'][ 'file' ] )){
			$this->log_file = $this->getFile($this->data['params'][ 'file' ]);
			$this->data['pagination_links'] = $this->CI->pagination->create_links();
			$this->data['current']['name'] = $this->data['params'][ 'file' ];
		}else{
			$this->log_file = $this->getFile($this->today);
			$this->data['pagination_links'] = $this->CI->pagination->create_links();
			$this->data['current']['name'] = $this->today;
		}
		
		$this->data['log_file']=$this->log_file;
		$this->CI->load->view( 'LogViewer', $this->data );
	}



	public function listFiles($limit=30){
		$list = get_dir_file_info( APPPATH . 'logs' );
		$filtered_list = array();

		foreach ($list as $file ){
			$file['attrs'] = '';
			$file['suffix'] = '';

			if( $file[ 'name' ] == $this->log_file ){
				$file['attrs'] = 'selected="selected"';
			}

			if( $file[ 'name' ] == $this->today ){
				$file[ 'suffix'] = ' - (' . $this->CI->lang->line( 'fire_log_today' ) . ')';
			}

			if( $file[ 'name' ] != 'index.html' ){
				array_push( $filtered_list, $file ); 
			}

		}
		usort($filtered_list, create_function('$a, $b', 'return strcmp($b["name"], $a["name"]);'));
		return array_slice($filtered_list,0,$limit);
	}

	public function getFile( $log_file ){

		$path = APPPATH . 'logs/' .$log_file;

		if( file_exists( $path )){
			$this->split_files( $path );

			//$config = $this->CI->config->item( 'fire_log_pagination_settings' );
			//$config['base_url'] = build_spark_url( $this->url_vals );
			$config['base_url'] = current_url().'?file='.$log_file.(isset($this->data['params'][ 'filter' ] )? '&filter='.$this->data['params'][ 'filter' ] :'');
			$config['total_rows'] = count( $this->pages );
			$config['uri_segment' ] = $this->CI->uri->total_segments();
			$config['enable_query_strings'] = true;
			$config['page_query_string'] = true;
			// this is hardcoded so it doesnt get overode from config
			$config[ 'per_page' ] = 1;


			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] =  '<li>';
			$config['last_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			


			$this->CI->pagination->initialize($config);
			$cur_page = $this->CI->input->get('per_page');

			if( !is_array( $cur_page ) ){
				$cur_page = intval( $cur_page );
			}else{
				$cur_page = 0;
			}

			//trace( $cur_page, TRUE );
			if( isset( $this->pages[ $cur_page ] )){
				$data = $this->pages[ $cur_page ];
			}else{
				$data = $this->CI->lang->line( 'fire_log_no_results_found' );
			}


			$data = ltrim( $data, "\n" );
			$data = ltrim( $data, "\n" );

			$data = str_replace( 'DEBUG', '<div class="debug">DEBUG', $data );
			$data = str_replace( 'ERROR', '<div class="error">ERROR', $data );
			$data = str_replace( 'INFO', '<div class="info">INFO', $data );
			$data = str_replace( "<div", "</div><div", $data );
			$data = str_replace( "if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>", "", $data );
			//				$data = ltrim( $data, '<div class="logContainer"></div>' );
		
			$data = substr($data, 15 );

		}else{
			$msg = $this->CI->lang->line( 'fire_log_not_found' );
			$msg = str_replace( '%log_file%', $this->log_file, $msg );
			$data = $msg;
		}


		return $data;
	}

	public function getParams(){
		$params['file']=$this->CI->input->get('file');
		$params['filter']=$this->CI->input->get('filter');

		return $params;
	}


	function split_files( $source, $lines=75 ){
		if( in_array($this->data['params']['filter'], array( 'debug','info','error' )) != FALSE ){
			$this->filter = TRUE;
		}

		$i=0;
		$buffer='';

		$handle = fopen( $source, "r" );

		while (!feof ($handle)){

			$content = fgets( $handle, 496);

			if( $this->CI->config->item( 'fire_log_strip_tags') ){
				$content = strip_tags( $content );
			}


			$buffer .= $content;
			$i++;

			if ($i >= $lines){
				array_push( $this->pages, $buffer );
				$buffer='';
				$i=0;
				$lines += 75;
			}
		}
		
		if ( !empty( $buffer )) {
			array_push( $this->pages, $buffer );
		}			

		fclose ($handle);


		if( $this->filter ){

			$temp_pages = array();
			$temp_page = '';

			foreach ($this->pages as $page )
			{

				$page = $this->filter( $page );

				if( strlen( $temp_page ) < 5000 )
				{
					$temp_page .= $page;
				}
				else
				{
					array_push( $temp_pages, $temp_page );
					$temp_page = '';
				}
			}

			if( !empty( $temp_page ))
			{
				array_push( $temp_pages, $temp_page );
			}

			$this->pages = $temp_pages;
		//	var_dump($this->pages);
		}
	}


	public function filter( $str ){
		$content = $str;

		switch ($this->data['params'][ 'filter' ]){
			case 'debug':	
			$content = preg_replace( "~INFO.*\n~", '', $content );
			$content = preg_replace( "~ERROR.*\n~", '', $content );
			break;
			case 'error':
			$content = preg_replace( "~INFO.*\n~", '', $content );
			$content = preg_replace( "~DEBUG.*\n~", '', $content );
			break;
			case 'info':
			$content = preg_replace( "~DEBUG.*\n~", '', $content );
			$content = preg_replace( "~ERROR.*\n~", '', $content );
			break;
		}
		return $content;
	}

}

?>