<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Log Viewer</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<style type="text/css" media="screen">
		body {padding-top: 20px; padding-bottom: 20px; background: #353635; }
		.navbar { margin-bottom: 20px; background: #353635; border: 0px;}
		.pagination{margin:0px;}
		.files{margin: 0px; padding: 0px;}
		.files .btn{background: #353635; border:0px; color:#fff;}
		.files .btn.active{background: #222;}
		.log_file .content{padding-right: 15px;}
		.pagination li a{background: #353635; border:0px; color:#fff;}
		.pagination li.active a{background: #222;}
		.debug, .error, .info{ display:block; padding:4px; margin:0; border:1px solid #444; border-style:outset; border-left:none; border-right:none; white-space: pre-wrap; }
		.debug{ color:#8f9d6a; background-color:#353635; }
		.error{color:#c26549; background-color:#44393f; }
		.info{	color:#d8ce84; background-color:#444439; }
	</style>


</head>
<body>
	<div class="container">


      <!-- Static navbar -->
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand" href="#">2011-06-15</span>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav pull-right">
              <li class="<?php echo ($params['filter']=='none' or $params['filter']=='')? 'active':''; ?>">
              	<a href="/admin/apilog/platformlogs<?php echo (isset($params['file']) && $params['file'])? '?file='.$params['file'].'&':'?'; ?>filter=none">
              		SHOW ALL</a></li>
              <li class="<?php echo ($params['filter']=='error')? 'active':''; ?>" >
              	<a href="/admin/apilog/platformlogs<?php echo (isset($params['file']) && $params['file'])? '?file='.$params['file'].'&':'?'; ?>filter=error">ERRORS</a></li>
              <li class="<?php echo ($params['filter']=='info')? 'active':''; ?>" >
              	<a href="/admin/apilog/platformlogs<?php echo (isset($params['file']) && $params['file'])? '?file='.$params['file'].'&':'?'; ?>filter=info">INFO</a></li>
              <li class="<?php echo ($params['filter']=='debug')? 'active':''; ?>">
              	<a href="/admin/apilog/platformlogs<?php echo (isset($params['file']) && $params['file'])? '?file='.$params['file'].'&':'?'; ?>filter=debug">DEBUG</a></li>
              
            </ul>
            
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="container">
        <div class="row">
        	<div class="col-xs-1 files">
        		<div class="btn-group-vertical" role="group" aria-label="">
					<!-- <button type="button" class="btn btn-inverse">2011-02-14</button> -->
					<?php foreach ($files as $key => $file) {	?>
							<a href="/admin/apilog/platformlogs?file=<?php echo $file['name']; ?>" class="btn btn-default <?php echo ($file['name']==$current['name'])? 'active':''; ?>">
								<?php echo substr($file['name'],4,-4); ?></a>
					<?php } ?>
				</div>
        	</div>
        	<div class="col-xs-11 log_file">
        		<?php if($pagination_links!=''){ ?>
	        		<div >
	        			<nav>
						  <ul class="pagination">			
						  	<?php echo ($pagination_links); ?>
						  </ul>
						</nav>
					</div>
				<?php } ?>
        		<div class="content">
	        		<?php echo $log_file; ?>
	        	</div>
        	</div>
        </div>
      </div>

    </div> <!-- /container -->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</body>
</html>